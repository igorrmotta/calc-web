import React, {Component} from 'react';
import './CalculatorPage.css';
import './MainPage.css';
import TopBar from './TopBar.js';

class CalculatorPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            navigator: props.navigator
        }
    }

    onPensionAmountsClick() {
        this.state.navigator.setNavigation('pensionAmountsFormPage');
    }

    render() {
        var mainStyle = {
            width: '100vw',
            height: '100vh',
            backgroundColor: '#051029',
        };

        var pageWrapStyle = {
            width: '100vw',
            height: '100vh',
            display: 'flex',
            flexDirection: 'column'
        };

        var headerLogoStyle = {
            height: '72px',
            marginTop: '18px',
            marginBottom: '18px',
        };

        var contentStyle = {
            backgroundColor: '#e7eaf0',
            height: '100%',
            display: 'flex',
            justifyContent: 'flex-start',
            alignItems: 'center',
            flexDirection: 'column',
            overflow: 'scroll',
            paddingBottom: '18px'
        };

        return (<div id="outer-container" style={mainStyle}>
                <main id="page-wrap" style={pageWrapStyle}>
                    <TopBar navigator={this.state.navigator} showBackButton={true}/>
                    <div style={contentStyle}>
                        <span className="contentTitle">Calculator Options</span>
                        <div className="calculatorOptionButton" onClick={() => this.onPensionAmountsClick() }>
                            <span>Pension Amounts</span>
                        </div>
                        <div className="calculatorOptionButton">
                            <span>Contribution Limits</span>
                        </div>
                        <div className="calculatorOptionButton">
                            <span>Tax Rates</span>
                        </div>
                        <div className="calculatorOptionButton">
                            <span>Preservation Age</span>
                        </div>
                        <div className="calculatorOptionButton">
                            <span>Tax Payable</span>
                        </div>
                        <div className="calculatorOptionButton">
                            <span>Death Benefits</span>
                        </div>
                    </div>
                </main>
        </div>);
    }
}

export default CalculatorPage;