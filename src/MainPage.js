import React, {Component} from 'react';
import './MainPage.css';
import TopBar from './TopBar.js';

class MainPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            navigator: props.navigator
        }
    }

    onCalculatorClick() {
        this.state.navigator.setNavigation('calculatorPage');
    }

    render() {
        var mainStyle = {
            width: '100vw',
            height: '100vh',
            backgroundColor: '#051029',
        };

        var pageWrapStyle = {
            width: '100vw',
            height: '100vh',
            display: 'flex',
            flexDirection: 'column'
        };

        var contentStyle = {
            backgroundColor: '#e7eaf0',
            height: '100%',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
        };

        return (<div style={mainStyle}>
            <main style={pageWrapStyle}>
                <TopBar navigator={this.state.navigator} showBackButton={false}/>
                <div style={contentStyle}>
                    <div className="mainButton" onClick={() => this.onCalculatorClick() }>
                        <img className="icon" src={require('./assets/calculator-icon.png') }/>
                        <span className="text" >Calculator</span>
                    </div>
                    <div className="mainButton">
                        <img className="icon" src={require('./assets/subscribe-icon.png') }/>
                        <span className="text">Subscribe</span>
                    </div>
                </div>
            </main>
        </div>);
    }
}

export default MainPage;