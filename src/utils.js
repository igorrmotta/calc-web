/**
  * Decimal adjustment of a number.
  *
  * @param {String}  type  The type of adjustment.
  * @param {Number}  value The number.
  * @param {Integer} exp   The exponent (the 10 logarithm of the adjustment base).
  * @returns {Number} The adjusted value.
  */
function decimalAdjust(type, value, exp) {
    // If the exp is undefined or zero...
    if (typeof exp === 'undefined' || +exp === 0) {
        return Math[type](value);
    }
    value = +value;
    exp = +exp;
    // If the value is not a number or the exp is not an integer...
    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
        return NaN;
    }
    // Shift
    value = value.toString().split('e');
    value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
}

// Decimal round
function round10(value, exp) {
    return decimalAdjust('round', value, exp);
}

function floor10(value, exp) {
    return decimalAdjust('floor', value, exp);
}

function ceil10(value, exp) {
    return decimalAdjust('ceil', value, exp);
}

Date.prototype.yyyymmdd = function() {
  var mm = this.getMonth() + 1; // getMonth() is zero-based
  var dd = this.getDate();

  return [this.getFullYear(),'-', !mm[1] && '0', mm,'-', !dd[1] && '0', dd].join(''); // padding
};

Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

function convertStringDateToDateObject(strDate) {
    if (strDate instanceof Date)
        return strDate;
    //'DD-MM-YYYY'
    // Make it sure it's working for iOS
    var strArray = strDate.split('-');
    var day = parseInt(strArray[0]);
    var month = parseInt(strArray[1]);
    var year = parseInt(strArray[2]);
    var ret = new Date(year, month - 1, day);
    return ret;
}

function getAge(strDateOfBirth) {
    var dateOfBirth = convertStringDateToDateObject(strDateOfBirth);
    var today = new Date();
    return today.getFullYear() - dateOfBirth.getFullYear();
}

export {convertStringDateToDateObject};
export {getAge};
export {round10};
export {floor10};
export {ceil10};
