import React, { Component } from 'react';
import './App.css';
import CoverPage from './CoverPage.js';
import MainPage from './MainPage.js';
import CalculatorPage from './CalculatorPage.js';
import PensionAmountsFormPage from './CalculatorPages/PensionAmountsFormPage.js';
import PensionAmountsResultPage from './CalculatorPages/PensionAmountsResultPage.js';

class Navigator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentNavigation: props.initialNavigation,
      onNavigationChange: props.onNavigationChange,
      history: []
    }
  }

  getCurrentNavigation() {
    return this.state.currentNavigation;
  }

  goBack() {
    var history = this.state.history;
    history.pop();
    this.state.history = history;
    var current = history[history.length - 1];
    this.state.currentNavigation = current;
    this.state.onNavigationChange(current);
  }

  setNavigation(to) {
    var history = this.state.history;
    history.push(to);
    this.state.history = history;
    this.state.currentNavigation = to;
    this.state.onNavigationChange(to);
  }
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 'cover',
      navigator: new Navigator({ initialNavigation: 'cover', onNavigationChange: this.onNavigationChange.bind(this) })
    }
  }

  onNavigationChange(to) {
    console.log('navigating to: ' + to);
    this.setState({ currentPage: to });
  }

  render() {
    if (this.state.currentPage === 'cover')
      return (<CoverPage navigator={this.state.navigator}/>);
    else if (this.state.currentPage === 'mainPage')
      return (<MainPage navigator={this.state.navigator}/>)
    else if (this.state.currentPage === 'calculatorPage')
      return (<CalculatorPage navigator={this.state.navigator} />);
    else if (this.state.currentPage === 'pensionAmountsFormPage')
      return (<PensionAmountsFormPage navigator={this.state.navigator} />);
    else if (this.state.currentPage === 'pensionAmountsResultPage')
      return (<PensionAmountsResultPage navigator={this.state.navigator} />);
    else {
      console.log('ooooooops!!!');
      return <div></div>;
    }
  }
}

export default App;
