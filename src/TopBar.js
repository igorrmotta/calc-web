import React, {Component} from 'react';
import './TopBar.css';
var BurgerMenu = require('react-burger-menu').bubble;

class TopBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            navigator: props.navigator,
            showBackButton: props.showBackButton,
        }
    }

    onBackPress(){
        this.state.navigator.goBack();
    }

    render() {
        var headerLogoStyle = {
            height: '72px',
            marginTop: '18px',
            marginBottom: '18px',
        };
        
        return (
            <div>
                <header className="header">
                    <img style={headerLogoStyle} src={require('./assets/white-logo.svg') }/>
                    {(this.state.showBackButton) && <img className="backIcon" onClick={()=>this.onBackPress()}/>}
                </header>
                <div className='right'>
                    <BurgerMenu className={'right'} pageWrapId={"page-wrap"} outerContainerId={"outer-container"} right>
                        <a id="home" className="menu-item" href="#">Home</a>
                        <a id="about" className="menu-item" href="#">About</a>
                        <a id="contact" className="menu-item" href="#">Contact</a>
                    </BurgerMenu>
                </div>
            </div>
        );
    }
}

export default TopBar;

