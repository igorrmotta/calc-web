import React from 'react';
import './CoverPage.css';


class CoverPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            navigator: props.navigator
        }
    }

    componentDidMount() {
        var _this = this;
        setTimeout(function () {
            if (_this.refs.logo)
                _this.refs.logo.className += ' move-out';
        }, 3000);

        // setTimeout(function(){
        //      _this.state.navigator.setNavigation('mainPage');
        // }, 5000);
        
        //UNCOMMENT FOR DEVELOPMENT AGILITY
        setTimeout(function () {
            _this.state.navigator.setNavigation('mainPage');
        }, 1000);
    }

    render() {
        var mainStyle = {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            width: '100vw',
            height: '100vh',
            backgroundColor: '#051029',
        };

        var logoStyle = {
            width: '50vw'
        };

        return (<div style={mainStyle}>
            <img ref="logo" className="logo" style={logoStyle} src={require('./assets/white-logo.svg') }/>
        </div>);
    }
}

export default CoverPage;