import React, {Component} from 'react';
import '../MainPage.css';
import '../CalculatorPage.css';
import './PensionAmountsPage.css';
import TopBar from '../TopBar.js';

class PensionAmountsFormPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            navigator: props.navigator,
            dateOfBirth: '',
            datePensionCommences: new Date().yyyymmdd(),
            balance: '',
            bTTR: false
        }
    }

    onCalculatePress() {
        //TODO validate if all inputs required were filled
        this.state.navigator.setNavigation('pensionAmountsResultPage', {
            dateOfBirth: this.state.dateOfBirth,
            datePensionCommences: this.state.datePensionCommences,
            balance: this.state.balance,
            bTTR: this.state.bTTR
        });
    }

    onDateOfBirthChange(value) {
        this.setState({ dateOfBirth: value });
    }

    onDatePensionCommencesChange(value) {
        this.setState({ datePensionCommences: value });
    }

    onBalanceChange(value) {
        this.setState({ balance: value });
    }

    onTransitionToRetirementChange(value) {
        this.setState({ bTTR: value });
    }

    renderForm() {
        return (
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
                <span className="contentTitle">Pension Amounts</span>
                <div className="card">
                    <div className="contentField">
                        <span>Date of Birth</span>
                        <div className="contentWrapper">
                            <object ref="dateOfBirthCalendarIcon" className="calendarIcon">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 20">
                                    <title>calendar-icon</title>
                                    <path d="M6,9H4v2H6Zm4,0H8v2h2Zm4,0H12v2h2Zm2-7H15V0H13V2H5V0H3V2H2A2,2,0,0,0,0,4V18a2,2,0,0,0,2,2H16a2,2,0,0,0,2-2V4A2,2,0,0,0,16,2Zm0,16H2V7H16Z"/>
                                </svg>
                            </object>
                            <input type="date" className="date"
                                onChange={(event) => this.onDateOfBirthChange(event.target.value) }
                                onFocus={() => { this.refs.dateOfBirthCalendarIcon.className = "calendarIconFocus" } }
                                onBlur={() => { this.refs.dateOfBirthCalendarIcon.className = "calendarIcon" } } placeholder="10/04/2010"/>
                        </div>
                    </div>

                    <div className="contentField">
                        <span>Date Pension Commences</span>
                        <div className="contentWrapper">
                            <object ref="pensionCalendarIcon" className="calendarIcon">
                                <svg  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 20">
                                    <title>calendar-icon</title>
                                    <path
                                        d="M6,9H4v2H6Zm4,0H8v2h2Zm4,0H12v2h2Zm2-7H15V0H13V2H5V0H3V2H2A2,2,0,0,0,0,4V18a2,2,0,0,0,2,2H16a2,2,0,0,0,2-2V4A2,2,0,0,0,16,2Zm0,16H2V7H16Z"/>
                                </svg>
                            </object>
                            <input type="date" className="date" defaultValue={this.state.datePensionCommences}
                                onChange={(event) => this.onDatePensionCommencesChange(event.target.value) }
                                onFocus={() => { this.refs.pensionCalendarIcon.className = "calendarIconFocus" } }
                                onBlur={() => { this.refs.pensionCalendarIcon.className = "calendarIcon" } } placeholder="10/04/2010"/>
                        </div>
                    </div>

                    <div className="contentField">
                        <span>Balance</span>
                        <div className="contentWrapper">
                            <object ref="balanceMoneyIcon" className="moneyIcon">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10.18 18">
                                    <title>money-icon</title>
                                    <path d="M5.48,7.9c-2.27-.59-3-1.2-3-2.15s1-1.85,2.7-1.85,2.44.85,2.5,2.1H9.89A4,4,0,0,0,6.68,2.19V0h-3V2.16C1.74,2.58.18,3.84.18,5.77c0,2.31,1.91,3.46,4.7,4.13,2.5.6,3,1.48,3,2.41,0,.69-.49,1.79-2.7,1.79s-2.87-.92-3-2.1H0c.12,2.19,1.76,3.42,3.68,3.83V18h3V15.85c1.95-.37,3.5-1.5,3.5-3.55C10.18,9.46,7.75,8.49,5.48,7.9Z"/>
                                </svg>
                            </object>
                            <input className="text" type="number" value={this.balance}
                                onChange={(event) => this.onBalanceChange(event.target.value) }
                                onFocus={() => { this.refs.balanceMoneyIcon.className = "moneyIconFocus" } }
                                onBlur={() => { this.refs.balanceMoneyIcon.className = "moneyIcon" } }/>
                        </div>
                    </div>

                    <div className="contentField">
                        <div className="contentWrapper" style={{ alignItems: 'center', justifyContent: 'center' }}>
                            <input className="checkbox" type='checkbox' defaultValue={this.state.bTTR}
                                onChange={(event) => this.onTransitionToRetirementChange(event.target.checked) }/>
                            <span style={{ marginBottom: 0 }}>Transition to Retirement</span>
                        </div>
                    </div>

                    <div className="contentField">
                        <button className="button" onClick={() => this.onCalculatePress() }>
                            <span>GO</span>
                        </button>
                    </div>
                </div>
            </div>);
    }

    render() {
        var mainStyle = {
            width: '100vw',
            height: '100vh',
            backgroundColor: '#051029',
        };

        var pageWrapStyle = {
            width: '100vw',
            height: '100vh',
            display: 'flex',
            flexDirection: 'column'
        };

        var headerLogoStyle = {
            height: '72px',
            marginTop: '18px',
            marginBottom: '18px',
        };

        var contentStyle = {
            backgroundColor: '#e7eaf0',
            height: '100%',
            display: 'flex',
            justifyContent: 'flex-start',
            alignItems: 'center',
            flexDirection: 'column',
            overflow: 'scroll',
            paddingBottom: '18px'
        };

        let date = '2017-04-24'
        return (<div id="outer-container" style={mainStyle}>
            <main id="page-wrap" style={pageWrapStyle}>
                <TopBar navigator={this.state.navigator} showBackButton={true}/>
                <div style={contentStyle}>
                    {this.renderForm() }
                </div>
            </main>
        </div>);
    }
}

export default PensionAmountsFormPage;
