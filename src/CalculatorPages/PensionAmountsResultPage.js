import React, {Component} from 'react';
import '../MainPage.css';
import '../CalculatorPage.css';
import './PensionAmountsPage.css';
import TopBar from '../TopBar.js';
import {getAge, convertStringDateToDateObject} from '../utils.js';

class PensionAmountsResultPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            navigator: props.navigator,
            dateOfBirth: props.dateOfBirth,
            datePensionCommences: props.datePensionCommences,
            balance: props.balance,
            bTTR: props.bTTR,
            result: {}
        }
    }

    getInformationOfMinimumPayment(age) {
        if (age >= 95) {
            return {
                age: '95+',
                percentage: 0.14
            }
        } else if (age >= 90) {
            return {
                age: '90-94',
                percentage: 0.11
            }
        } else if (age >= 85) {
            return {
                age: '85-89',
                percentage: 0.09
            }
        } else if (age >= 80) {
            return {
                age: '80-84',
                percentage: 0.07
            }
        } else if (age >= 75) {
            return {
                age: '75-79',
                percentage: 0.06
            }
        } else if (age >= 65) {
            return {
                age: '65-74',
                percentage: 0.05
            }
        } else if (age >= 55) {
            return {
                age: '55-64',
                percentage: 0.04
            }
        } else {
            //????????
            //Probably you are not allowed to have a pension yet!
            return {
                age: '55-',
                percentage: 0.04
            }
        }
    }

    getResult(commencementDate, bTTR, balance, minPaymentInfo) {
        var result = {};

        if (commencementDate.getMonth() <= 5) {
            console.log('before end of financial year');

            var minimumAmount;
            if (commencementDate.getMonth() === 5) {
                //but if start date is after 1 June, minimum pension is zero.
                minimumAmount = 0;
            } else {
                const commencementYear = commencementDate.getFullYear();
                const dateEndFinancialYear = new Date(commencementYear, 5, 30);

                const timeDiff = Math.abs(dateEndFinancialYear.getTime() - commencementDate.getTime());
                const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

                const previousEndFinancialyear = new Date(commencementYear - 1, 5, 30);
                const timeDiffFinancialYear = Math.abs(dateEndFinancialYear.getTime() - previousEndFinancialyear.getTime());
                const diffDaysFinancialYear = Math.ceil(timeDiffFinancialYear / (1000 * 3600 * 24));

                const rate = diffDays / diffDaysFinancialYear;
                minimumAmount = balance * minPaymentInfo.percentage * rate;
            }

            var maximumAmount = balance * 0.1;

            //Rounded to the nearest $10
            minimumAmount = round10(minimumAmount, 1);

            //Rounded Down to the nearest 1$
            maximumAmount = Math.floor(maximumAmount);

            result.minimumAmount = minimumAmount;
            result.maximumAmount = maximumAmount;
            result.minPaymentInfo = minPaymentInfo;
            result.bTTR = bTTR;
            result.balance = balance;
        } else {
            console.log('after end of financial year');
            var minimumAmount = balance * minPaymentInfo.percentage;
            var maximumAmount = balance * 0.1;

            //Rounded to the nearest $10
            minimumAmount = round10(minimumAmount, 1);

            //Rounded Down to the nearest 1$
            maximumAmount = Math.floor(maximumAmount);

            result.minimumAmount = minimumAmount;
            result.maximumAmount = maximumAmount;
            result.minPaymentInfo = minPaymentInfo;
            result.bTTR = bTTR;
            result.balance = balance;
        }
        return result;
    }

    componentWillMount() {
        const age = getAge(this.state.dateOfBirth);
        const minPaymentInfo = this.getInformationOfMinimumPayment(age);
        const balance = parseFloat(this.state.balance);

        var commencementDate = convertStringDateToDateObject(this.state.dateCommencement);
        var result = this.getResult(commencementDate, this.state.bTTR, balance, minPaymentInfo);

        this.setState = {
            result: result
        };
    }

    renderForm() {
        return (
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
                <span className="contentTitle">Pension Amounts</span>
                <div className="card">
                    <header>
                        <span>Your estimated annual <span style={{ fontWeight: '800', color: '#385392' }}>maximum </span>payment</span>
                        <h1>{'$' + this.state.result.balance.formatMoney(0) }</h1>
                        <span>Your estimated annual <span style={{ fontWeight: '800', color: '#385392' }}>minimum </span>payment</span>
                        <h1>{'$' + this.state.result.balance.formatMoney(0) }</h1>
                    </header>
                    <footer>
                        <div className="row" style={{ backgroundColor: '#385392', borderRadius: 5, color: '#ffffff', justifyContent: 'center' }}>
                            <span>Based on the following</span>
                        </div>
                        <div className="row">
                            <span>Age Group</span>
                            <span>{this.state.result.minPaymentInfo.age}</span>
                        </div>
                        <div className="row">
                            <span>Balance</span>
                            <span>{this.state.result.balance.formatMoney(0) }</span>
                        </div>
                        <div className="row" style={{ border: 'none' }}>
                            <span>Transition to Retirement</span>
                            <span>{this.state.result.bTTR ? 'Yes' : 'No'}</span>
                        </div>
                    </footer>
                </div>
                <span> For more information, refer to <span style={{ fontWeight: '800', textDecoration: 'underline' }}> table</span></span>
            </div>);
    }

    render() {
        var mainStyle = {
            width: '100vw',
            height: '100vh',
            backgroundColor: '#051029',
        };

        var pageWrapStyle = {
            width: '100vw',
            height: '100vh',
            display: 'flex',
            flexDirection: 'column'
        };

        var headerLogoStyle = {
            height: '72px',
            marginTop: '18px',
            marginBottom: '18px',
        };

        var contentStyle = {
            backgroundColor: '#e7eaf0',
            height: '100%',
            display: 'flex',
            justifyContent: 'flex-start',
            alignItems: 'center',
            flexDirection: 'column',
            overflow: 'scroll',
            paddingBottom: '18px'
        };

        let date = '2017-04-24'
        return (<div id="outer-container" style={mainStyle}>
            <main id="page-wrap" style={pageWrapStyle}>
                <TopBar navigator={this.state.navigator} showBackButton={true}/>
                <div style={contentStyle}>
                    {this.renderForm() }
                </div>
            </main>
        </div>);
    }
}

export default PensionAmountsResultPage;